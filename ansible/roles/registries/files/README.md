# Container Registries

Caching on the CI gateway the containers needed by DUTs is an effective way to
reduce Internet bandwidth needs and speed up execution of container-based jobs.

By default, CI-tron comes with a set of registries:

 * A proxy registry to https://registry.freedesktop.org
 * A local registry, for locally-built images

## Adding registries

You may add more registries by adding configuration files in
`/config/registries/`, named `${port}_${registry_name}.yml.j2`. You may use
`8100_quay.yml.j2.example` as a basis, then modify it according to the
[registry configuration](https://distribution.github.io/distribution/about/configuration/#list-of-configuration-options).

WARNING: Choose the name of your registry carefully, because you won't be able
to change it without modifying every job description that depends on it!

NOTE: Changes done in `/config/registries/` are reflected in real time. Run
`journalctl -fu registryd` to follow what is going on.

## Using a registry

Registries can referenced by name in a job description `{{ ${name}_registry }}`.

For example, if you want to reference the registry configured at
`/config/registries/8100_quay.yml.j2`, you may use it in a job description like
that: `b2c.run="-ti --tls-verify=false docker://{{ quay_registry }}/containers/buildah:latest"`.

## Renaming or removing a registry

WARNING: Renaming or removing a registry will break every single job
description that depends on it, so be careful!

To rename/remove a registry, just use `mv` or `rm` on the associated
configuration in `/config/registries/`. That's it :)
