SHELL := /bin/bash
.SHELLFLAGS := -eu -o pipefail -c
.ONESHELL:

# TODO: Integrate Vivian as a standard part of the container build process, rather than as a side-project.
VIVIAN := ./vivian/vivian
HOST ?= localhost
ifeq ($(HOST), localhost)
	SSH_PORT ?= 60022
else
	SSH_PORT ?= 22
endif
ifdef SSH_ID_KEY
	VIVIAN_SSH_KEY_OPT=--ssh-id=$(SSH_ID_KEY)
	SSH_KEY_OPT=-i $(SSH_ID_KEY) -o IdentitiesOnly=yes
	ANSIBLE_SSH_KEY_OPT=--private-key $(SSH_ID_KEY) --ssh-common-args "-o IdentitiesOnly=yes"
endif
V ?= 0
GITLAB_URL ?= "https://gitlab.freedesktop.org"
PRIV_MAC=$(shell printf "DE:AD:BE:EF:%02X:%02X\n" $$((RANDOM%256)) $$((RANDOM%256)))
PUBLIC_MAC=$(shell printf "DE:AD:BE:EF:%02X:%02X\n" $$((RANDOM%256)) $$((RANDOM%256)))
B2C_VERSION=v0.9.15
ARM32_CROSS_COMPILER ?= arm-none-eabi-
ARM64_CROSS_COMPILER ?= aarch64-linux-gnu-
UPLOAD ?= 0
UNAME_ARCH = $(shell uname -m)
ifeq ($(UNAME_ARCH), x86_64)
	GOARCH = amd64
	LINUX_ARCH = x86_64
else ifeq ($(UNAME_ARCH), aarch64)
	GOARCH = arm64
	LINUX_ARCH = arm64
else
	$(error "Unsupported architecture: $(UNAME_ARCH)")
endif

# Default container-related variables
CI_REGISTRY ?= localhost:8088
CI_PROJECT_PATH ?= gfx-ci/ci-tron
FDO_DISTRIBUTION_TAG ?= latest
export CI_REGISTRY CI_PROJECT_PATH FDO_DISTRIBUTION_TAG

ROOT_DIR := $(PWD)
TMP_DIR := $(PWD)/tmp

$(TMP_DIR)/boot2container-$(B2C_VERSION)-linux_$(GOARCH).cpio.xz:
	mkdir -p $(TMP_DIR)
	wget --quiet -O $@ https://gitlab.freedesktop.org/gfx-ci/boot2container/-/releases/$(B2C_VERSION)/downloads/initramfs.linux_$(GOARCH).cpio.xz

$(TMP_DIR)/linux-b2c-$(B2C_VERSION)-$(LINUX_ARCH):
	mkdir -p $(TMP_DIR)
	wget --quiet -O $@ https://gitlab.freedesktop.org/gfx-ci/boot2container/-/releases/$(B2C_VERSION)/downloads/linux-$(LINUX_ARCH)

$(TMP_DIR)/efi-aarch64-pflash.qcow2:
	mkdir -p $(TMP_DIR)

	path="/usr/share/edk2/aarch64/QEMU_EFI-pflash.qcow2"
	if [ -f "$$path" ]; then
		cp "$$path" $@
	else
		wget --quiet -O $@ https://gitlab.freedesktop.org/api/v4/projects/8246/packages/generic/qemu_efi_aarch64/0.0.1/QEMU_EFI-pflash.qcow2
	fi

$(TMP_DIR)/ipxe-disk.img $(TMP_DIR)/disk.img:
	mkdir -p $(TMP_DIR)
	qemu-img create -f qcow2 $@ 20G


$(TMP_DIR)/yq:
	mkdir -p $(TMP_DIR)
	wget --quiet -O $@ https://github.com/mikefarah/yq/releases/download/v4.41.1/yq_linux_$(GOARCH)
	chmod +x $@

.PHONY: local-registry
local-registry:
ifndef SKIP_LOCAL_REGISTRY
	@case "$(shell podman ps -a --format '{{.Status}}' --filter name=\^vivian_registry\$)" in
		Up*)
			echo "The local registry container already started"
			;;
		Exited* | Created*)
			@podman start vivian_registry
			;;
		*)
			@podman run -d -p 8088:5000 --replace --name vivian_registry docker.io/library/registry:2
			;;
	esac
endif

.PHONY: gateway
gateway: local-registry $(TMP_DIR)/yq
	BASE_IMAGE=$(BASE_IMAGE)
	if [ -z "$(BASE_IMAGE)" ]; then
		GATEWAY_BASE_IMAGE_TAG=$(shell $(TMP_DIR)/yq -r '.variables.GATEWAY_BASE_IMAGE_TAG' .gitlab-ci.yml)
		BASE_IMAGE=registry.freedesktop.org/gfx-ci/ci-tron/gateway-base:$$GATEWAY_BASE_IMAGE_TAG
	fi

	env \
	   BASE_IMAGE=$$BASE_IMAGE \
	   FDO_REPO_SUFFIX=gateway \
	   ANSIBLE_EXTRA_ARGS='--extra-vars service_mgr_override=inside_container' \
	   buildah unshare -- containers/gateway-build.sh

.PHONY: gateway-base
gateway-base: local-registry $(TMP_DIR)/yq
	BASE_IMAGE=$(BASE_IMAGE)
	if [ -z "$(BASE_IMAGE)" ]; then
		BASE_IMAGE=$(shell $(TMP_DIR)/yq -r '.".gateway-base".variables.BASE_IMAGE' .gitlab-ci.yml)
	fi

	env \
	   BASE_IMAGE=$$BASE_IMAGE \
	   FDO_REPO_SUFFIX=gateway-base \
	   ANSIBLE_EXTRA_ARGS='--extra-vars service_mgr_override=inside_container' \
	   buildah unshare -- containers/gateway-base-build.sh

.PHONY: machine-registration
machine-registration:
	env \
	   FDO_DISTRIBUTION_PLATFORMS='linux/amd64 linux/arm64/v8 linux/arm/v6 linux/riscv64' \
	   FDO_REPO_SUFFIX=machine-registration \
	   buildah unshare -- containers/machine-registration-build.sh

.PHONY: telegraf
telegraf:
	env \
	   FDO_REPO_SUFFIX=telegraf \
	   FDO_DISTRIBUTION_PLATFORMS="$(shell $(TMP_DIR)/yq -r '.telegraf.variables.FDO_DISTRIBUTION_PLATFORMS' .gitlab-ci.yml)" \
	   ALPINE_VERSION=$(shell $(TMP_DIR)/yq -r '.telegraf.variables.ALPINE_VERSION' .gitlab-ci.yml) \
	   TELEGRAF_VERSION=$(shell $(TMP_DIR)/yq -r '.telegraf.variables.TELEGRAF_VERSION' .gitlab-ci.yml) \
	   buildah unshare -- containers/telegraf-build.sh

# Run the CI-tron multi-service container inside a VM for local testing.
.PHONY: vivian
vivian: $(TMP_DIR)/disk.img
vivian:
	export
	$(MAKE) VIVIAN_CMD="start" vivian-run

.PHONY: vivian-integration-tests
vivian-integration-tests: FARM_NAME ?= "integration-tests-gateway"
vivian-integration-tests: VIVIAN_OPTS ?= "--tests-run-ansible"
vivian-integration-tests:
	@mkdir -p $(TMP_DIR)/appconfig
	@echo "VALVE_INFRA_FOLLOW_LOG=executor" > $(TMP_DIR)/appconfig/bash_login.env
	export
	$(MAKE) VIVIAN_CMD="integration-tests" VIVIAN_OPTS="$(VIVIAN_OPTS)" vivian-run

# Run vivian with VIVIAN_CMD
.PHONY: vivian-run
vivian-run: FARM_NAME ?= "vivian-$(USER)-gateway"
vivian-run: $(TMP_DIR)/boot2container-$(B2C_VERSION)-linux_$(GOARCH).cpio.xz $(TMP_DIR)/linux-b2c-$(B2C_VERSION)-$(LINUX_ARCH)
ifeq ($(UNAME_ARCH), aarch64)
vivian-run: $(TMP_DIR)/efi-aarch64-pflash.qcow2
endif
vivian-run: IMAGE_NAME ?= "localhost:8088/gfx-ci/ci-tron/gateway:latest"
vivian-run: VIVIAN_IMAGE_NAME = $(subst localhost,10.0.2.2,$(IMAGE_NAME))
vivian-run: local-registry
vivian-run:
ifndef VIVIAN_CMD
	$(error "VIVIAN_CMD needs to be set")
endif
ifndef SKIP_LOCAL_REGISTRY
	podman image exists $(IMAGE_NAME) || $(MAKE) -j1 gateway
ifneq (,$(findstring localhost:8088/,$(IMAGE_NAME)))
	podman push --tls-verify=false $(IMAGE_NAME)
endif
endif
	@mkdir -p $(TMP_DIR)/appconfig
	@echo "VALVE_INFRA_SSH_INSECURE=true" > $(TMP_DIR)/appconfig/sshd.env
ifeq ($(UNAME_ARCH), aarch64)
	export QEMU_EFI=$(TMP_DIR)/efi-aarch64-pflash.qcow2
endif
	@env \
	   GITLAB_URL=$(GITLAB_URL) \
	   GITLAB_REGISTRATION_TOKEN=$(GITLAB_REGISTRATION_TOKEN) \
	   $(VIVIAN) $(VIVIAN_OPTS) $(VIVIAN_SSH_KEY_OPT) --kernel-img=$(TMP_DIR)/linux-b2c-$(B2C_VERSION)-$(LINUX_ARCH) --ramdisk=$(TMP_DIR)/boot2container-$(B2C_VERSION)-linux_$(GOARCH).cpio.xz --gateway-disk-img=$(TMP_DIR)/disk.img --kernel-append='b2c.filesystem="vivianconfig_fs,type=9p,src=local-share" b2c.volume="vivianconfig_vol,filesystem=vivianconfig_fs" b2c.volume="ci-tron-cache" b2c.volume="ci-tron-config" b2c.hostname=$(FARM_NAME) b2c.container="-t --quiet --dns=none -v ci-tron-cache:/cache -v ci-tron-config:/config -v vivianconfig_vol:/config/vivian --tls-verify=false --entrypoint=/sbin/init docker://$(VIVIAN_IMAGE_NAME)" b2c.ntp_peer=auto b2c.pipefail b2c.cache_device=/dev/vda net.ifnames=0 quiet' --local-share=$(TMP_DIR)/appconfig $(VIVIAN_CMD)

# Start a production test of the virtual gateway. It will retrieve
# boot configuration from an external PXE server, booting from the
# production ISOs.
.PHONY: vivian-ipxe
vivian-ipxe: $(TMP_DIR)/ipxe-disk.img
ifndef IPXE_ISO
	$(error "IPXE_ISO needs to point at the installer image")
endif
	$(VIVIAN) $(VIVIAN_OPTS) test-installer --iso=$(IPXE_ISO) --gateway-disk-img=$(TMP_DIR)/ipxe-disk.img

# Create/add a new virtual DUT on the gateway's private network
.PHONY: vivian-add-dut
vivian-add-dut: OUTLET ?= 0
vivian-add-dut:
	curl --no-progress-meter -X POST localhost:8000/api/v1/dut/discover -H 'Content-Type: application/json' -d '{"pdu": "VPDU", "port_id": "${OUTLET}"}'

# Create/add a new virtual DUT on the gateway's private network
.PHONY: vivian-add-dut
vivian-remove-dut:
ifndef MACHINE_ID
	$(error "MACHINE_ID is missing)
endif
	curl --no-progress-meter -X DELETE localhost:8000/api/v1/dut/$(MACHINE_ID)

# Connect to a locally running virtual gateway.
.PHONY: vivian-connect
vivian-connect:
ifndef SSH_KEY_OPT
	$(warning "You might find helpful to define SSH_KEY_OPT (or SSH_ID_KEY) for this target.")
endif
	ssh root@$(HOST) $(SSH_KEY_OPT) -p $(SSH_PORT) -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null

.PHONY: vivian-provision
vivian-provision:
	if [ -n "$(TAGS)" ]; then _TAGS="-t $(TAGS)" ; else _TAGS="" ; fi
	cd ansible
	set -o pipefail
	ansible-playbook gateway.yml $(ANSIBLE_SSH_KEY_OPT) $$_TAGS -e ci_tron_root=$(CURDIR) -l vivian 0</dev/null |& cat -

.PHONY: live-provision
live-provision:
ifndef TARGET
	$(error "TARGET needs to point to the host you want to deploy to")
endif
	if [ -n "$(TAGS)" ]; then _TAGS="-t $(TAGS)" ; else _TAGS="" ; fi
	cd ansible
	ansible-playbook gateway.yml $(ANSIBLE_SSH_KEY_OPT) $$_TAGS -e ci_tron_root=$(CURDIR) -e target=$(TARGET) -l live

IPXE_DIR := $(TMP_DIR)/ipxe
$(IPXE_DIR):
	-mkdir -p $(TMP_DIR)
	git clone git://git.ipxe.org/ipxe.git $@


.PHONY: bootloaders
bootloaders: ipxe-dut-clients u-boot-binaries

.PHONY: ipxe-dut-clients
ipxe-dut-clients: $(IPXE_DIR)
	@# Tidy up the ipxe folder (overkill, but safety first)
	make -C $(IPXE_DIR)/src clean
	(cd $(IPXE_DIR) && git clean -fdx && git fetch && git reset --hard origin/master && git am $(ROOT_DIR)/patches/ipxe/*)

	cat <<'EOF'> $(IPXE_DIR)/src/config/general.h
	#ifndef CONFIG_GENERAL_H
	#define CONFIG_GENERAL_H

	FILE_LICENCE ( GPL2_OR_LATER_OR_UBDL );

	#include <config/defaults.h>

	#define BANNER_TIMEOUT		0
	#define ROM_BANNER_TIMEOUT	( 2 * BANNER_TIMEOUT )

	#define	NET_PROTO_IPV4		/* IPv4 protocol */

	#undef	DOWNLOAD_PROTO_TFTP	/* Trivial File Transfer Protocol */
	#define	DOWNLOAD_PROTO_HTTP	/* Hypertext Transfer Protocol */
	#undef	DOWNLOAD_PROTO_HTTPS	/* Secure Hypertext Transfer Protocol */
	#undef	DOWNLOAD_PROTO_FTP	/* File Transfer Protocol */
	#undef	DOWNLOAD_PROTO_SLAM	/* Scalable Local Area Multicast */
	#undef	DOWNLOAD_PROTO_NFS	/* Network File System Protocol */
	#undef	DOWNLOAD_PROTO_FILE	/* Local filesystem access */

	#undef	SANBOOT_PROTO_ISCSI	/* iSCSI protocol */
	#undef	SANBOOT_PROTO_AOE	/* AoE protocol */
	#undef	SANBOOT_PROTO_IB_SRP	/* Infiniband SCSI RDMA protocol */
	#undef	SANBOOT_PROTO_FCP	/* Fibre Channel protocol */
	#undef	SANBOOT_PROTO_HTTP	/* HTTP SAN protocol */

	#define	DNS_RESOLVER		/* DNS resolver */

	#define	NVO_CMD			/* Non-volatile option storage commands */
	#define	CONFIG_CMD		/* Option configuration console */
	#define CONSOLE_CMD		/* Console command */
	#define IMAGE_CMD		/* Image management commands */
	#define DHCP_CMD		/* DHCP management commands */
	#define IMAGE_ARCHIVE_CMD	/* Archive image management commands */

	#undef	ERRMSG_80211		/* All 802.11 error descriptions (~3.3kb) */

	#undef	BUILD_SERIAL
	#undef	BUILD_ID
	#undef	NULL_TRAP
	#undef	GDBSERIA
	#undef	GDBUDP

	#include <config/named.h>
	#include NAMED_CONFIG(general.h)
	#include <config/local/general.h>
	#include LOCAL_NAMED_CONFIG(general.h)

	#endif /* CONFIG_GENERAL_H */
	EOF

	cat <<'EOF'>$(IPXE_DIR)/src/config/console.h
	#ifndef CONFIG_CONSOLE_H
	#define CONFIG_CONSOLE_H

	FILE_LICENCE ( GPL2_OR_LATER_OR_UBDL );

	#include <config/defaults.h>

	#undef	CONSOLE_SERIAL		/* Serial port console */
	#define	CONSOLE_SYSLOG		/* Syslog console */
	#define	KEYBOARD_MAP	us
	#define	LOG_LEVEL	LOG_ALL

	#include <config/named.h>
	#include NAMED_CONFIG(console.h)
	#include <config/local/console.h>
	#include LOCAL_NAMED_CONFIG(console.h)

	#endif /* CONFIG_CONSOLE_H */
	EOF

	@# Compile the binaries
	artifact_prefix="`date +%F_%H-%M-%S`-`id -un`"
	binaries=""
	while read TARGET CROSS ARTIFACT_NAME DESCRIPTION; do
		make -C $(IPXE_DIR)/src -j`nproc` CROSS=$$CROSS $$TARGET NO_WERROR=1 || exit 1

		# Remove the surrounding quotes, if present
		DESCRIPTION="$${DESCRIPTION%\"}"
		DESCRIPTION="$${DESCRIPTION#\"}"

		target_path=$(IPXE_DIR)/src/$$TARGET
		binaries="$$binaries\n - $$DESCRIPTION: $$target_path"

		if [ $(UPLOAD) -eq 1 ]; then
			scp $$target_path gfx-ci.steamos.cloud:/var/www/downloads/ipxe-dut-client/$${artifact_prefix}-$$ARTIFACT_NAME
		fi
	done <<'EOD'
		bin/undionly.kpxe           ""                         i386-undionly.kpxe  "PCBIOS    "
		bin-i386-efi/snponly.efi    ""                         i386-snponly.efi    "i386 EFI  "
		bin-x86_64-efi/snponly.efi  ""                         x86_64-snponly.efi  "X86_64 EFI"
		bin-arm32-efi/snponly.efi   "$(ARM32_CROSS_COMPILER)"  arm32-snponly.efi   "ARM32 EFI "
		bin-arm64-efi/snponly.efi   "$(ARM64_CROSS_COMPILER)"  arm64-snponly.efi   "ARM64 EFI "
	EOD

	echo
	echo "########################################################################"
	echo
	echo "The compilation is now complete, you will find your binaries at:"
	echo
	echo -e "$$binaries"
	echo
	if [ $(UPLOAD) -eq 1 ]; then
	echo "They were all uploaded to https://downloads.gfx-ci.steamos.cloud/ipxe-dut-client/"
	else
	echo "Upload them to https://downloads.gfx-ci.steamos.cloud/ipxe-dut-client/ (or set UPLOAD=1)"
	fi
	echo
	echo "########################################################################"

UBOOT_DIR := $(TMP_DIR)/u-boot
$(UBOOT_DIR):
	-mkdir -p $(TMP_DIR)
	git clone https://github.com/u-boot/u-boot.git $@


.PHONY: u-boot-binaries
u-boot-binaries: $(UBOOT_DIR)
	@# Tidy up the u-boot folder (overkill, but safety first)
	(cd $(UBOOT_DIR) && \
		 git clean -fdx && \
		 git fetch && \
		 git reset --hard v2024.01 && \
		 git am $(ROOT_DIR)/patches/u-boot/*)

	artifact_prefix="`date +%F_%H-%M-%S`-`id -un`"
	binaries=""
	while read ARCH CROSS CONFIG DESCRIPTION; do
		$(MAKE) -C $(UBOOT_DIR) O=$(UBOOT_DIR)/bin-$${ARCH} CROSS_COMPILE=$$CROSS $$CONFIG || exit 1
		echo 'CONFIG_BOOTCOMMAND="if dhcp ; then source $${fileaddr} ; fi ; reset"' >> $(UBOOT_DIR)/bin-$${ARCH}/.config
		$(MAKE) -C $(UBOOT_DIR) O=$(UBOOT_DIR)/bin-$${ARCH} CROSS_COMPILE=$$CROSS u-boot.bin || exit 1

		# Remove the surrounding quotes, if present
		DESCRIPTION="$${DESCRIPTION%\"}"
		DESCRIPTION="$${DESCRIPTION#\"}"

		target_path=$(UBOOT_DIR)/bin-$${ARCH}/u-boot.bin
		binaries="$$binaries\n - $$DESCRIPTION: $$target_path"

		if [ $(UPLOAD) -eq 1 ]; then
			scp $$target_path gfx-ci.steamos.cloud:/var/www/downloads/u-boot/$${artifact_prefix}-u-boot.bin.$${ARCH}
		fi
	done <<'EOD'
		arm		$(ARM32_CROSS_COMPILER)	qemu_arm_defconfig	"Qemu ARM32"
		arm64		$(ARM64_CROSS_COMPILER)	qemu_arm64_defconfig	"Qemu ARM64"
	EOD
		#x86_64		''			qemu-x86_64_defconfig	"Qemu AMD64"

	echo
	echo "########################################################################"
	echo
	echo "The compilation is now complete, you will find your binaries at:"
	echo
	echo -e "$$binaries"
	echo
	if [ $(UPLOAD) -eq 1 ]; then
	echo "They were all uploaded to https://downloads.gfx-ci.steamos.cloud/u-boot/"
	else
	echo "Upload them to https://downloads.gfx-ci.steamos.cloud/u-boot/ (or set UPLOAD=1)"
	fi
	echo
	echo "########################################################################"

.PHONY: clean
clean:
	-rm -rf $(TMP_DIR) container_build.log local_sha gateway_monitor.sock
	# Stop and remove local registry container if it's running
	container=$(shell podman ps -a --format '{{.Names}}' | grep ^vivian_registry)
	if [ ! -z "$$container" ]; then
		podman stop vivian_registry
		podman rm -fi vivian_registry
	fi
